import React, { useEffect } from 'react'
import GoogleMapReact from 'google-map-react'
import Marker from './Marker'
import { useAppContext } from '../../context'
import './styles.css'

function Map () {
  const appContext = useAppContext()
  const { selectedIncident, incidents, setSelectedIncident } = appContext

  useEffect(() => {
    if (!selectedIncident && incidents.length) {
      setSelectedIncident(incidents[0])
    }
  }, [incidents, selectedIncident, setSelectedIncident])

  return (
    <div className='Map'>
      <GoogleMapReact
        bootstrapURLKeys={{
          key: 'AIzaSyBRvc8gfPkAAtxTCfA78yS1A_zAIrDXLqU',
          language: 'en',
          region: 'DE'
        }}
        defaultCenter={{
          lat: 0,
          lng: 0
        }}
        defaultZoom={1}
        {...(incidents.length && {
          center: {
            lat: selectedIncident ? selectedIncident.position.from.latitude : incidents[0].position.from.latitude,
            lng: selectedIncident ? selectedIncident.position.from.longitude : incidents[0].position.from.longitude
          }
        })}
        zoom={4}
        key={incidents}
        yesIWantToUseGoogleMapApiInternals
        onGoogleApiLoaded={handleGoogleMapApi(incidents, setSelectedIncident)}
      >
        {
          selectedIncident &&
            <Marker
              lat={selectedIncident.position.from.latitude}
              lng={selectedIncident.position.from.longitude}
              setSelectedIncident={setSelectedIncident}
              incident={selectedIncident}
              key={selectedIncident._id}
            />
        }
      </GoogleMapReact>
    </div>
  )
}

export function handleGoogleMapApi (incidents, setSelectedIncident) {
  return function (google) {
    const createPath = (path, color) => new google.maps.Polyline({
      path,
      strokeColor: color,
      strokeOpacity: 1.0,
      strokeWeight: 3
    })

    createShapesOnTheMap(incidents, setSelectedIncident, createPath, google)
  }
}
export function createShapesOnTheMap (incidents, setSelectedIncident, createPath, google) {
  incidents.forEach(incident => {
    const { from, to } = incident.position
    const { criticality } = incident.status
    const path = [{
      lat: from.latitude,
      lng: from.longitude
    }, {
      lat: to.latitude,
      lng: to.longitude
    }]

    const Shape = createPath(path, getColor(criticality))

    Shape.setMap(google.map)
    Shape.addListener('click', () => setSelectedIncident(incident))
  })
}

export function getColor (criticality) {
  let color

  if (criticality === 'critical') {
    color = 'red'
  } else if (criticality === 'warning') {
    color = 'orange'
  } else {
    color = 'green'
  }

  return color
}

export default Map
