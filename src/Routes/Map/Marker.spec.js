/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import { Link } from 'react-router-dom'
import Marker from './Marker'

const mockIncident = {
  _id: '5ed60f01f82566b3a5363c4f',
  status: {
    criticality: 'critical',
    verified: false
  },
  address: '554 Kensington Street, Weedville, Idaho, 8448'
}
const mockIncidentVerified = {
  _id: '5ed60f01d1c183b898c84a03',
  status: {
    criticality: 'warning',
    verified: true
  },
  address: '699 Jewel Street, Whitehaven, Iowa, 6558'
}

describe('<Marker />', () => {
  let wrapper
  let getWrapper
  const setSelectedIncident = jest.fn()

  beforeEach(() => {
    const props = {
      incident: mockIncident,
      setSelectedIncident
    }

    getWrapper = incident => shallow(<Marker {...props} incident={incident} />)
  })

  it('renders without crashing', () => {
    wrapper = getWrapper(mockIncident)

    wrapper.find(Link).simulate('click')
    expect(setSelectedIncident).toBeCalled()
  })

  it('renders with proper status and id', () => {
    wrapper = getWrapper(mockIncident)

    expect(wrapper.find('.Marker-meta > span').at(0).text()).toEqual('id: 5ed60f01f82566b3a5363c4f')
    expect(wrapper.find('.Marker-meta > span').at(1).text()).toEqual('criticality: critical')
    expect(wrapper.find('.Marker-meta > span').at(2).text()).toEqual('verified: false')
  })

  it('renders with proper status and id', () => {
    wrapper = getWrapper(mockIncidentVerified)

    expect(wrapper.find('.Marker-meta > span').at(0).text()).toEqual('id: 5ed60f01d1c183b898c84a03')
    expect(wrapper.find('.Marker-meta > span').at(1).text()).toEqual('criticality: warning')
    expect(wrapper.find('.Marker-meta > span').at(2).text()).toEqual('verified: true')
  })
})
