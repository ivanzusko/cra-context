import React from 'react'
import { Link } from 'react-router-dom'
import './styles.css'

function Marker ({ setSelectedIncident, incident }) {
  const {
    _id,
    address,
    status: { criticality, verified }
  } = incident

  return (
    <Link
      to={`/incident/${_id}`}
      onClick={() => setSelectedIncident(incident)}
      data-cy='map-marker'
    >
      <div className={`Marker Marker-${criticality}`}>
        <p className='Marker-title'>{address}</p>
        <div className='Marker-meta'>
          <span>id: <span data-cy-marker='marker-id'>{_id}</span></span>
          <span>criticality: {criticality}</span>
          <span>verified: {verified ? 'true' : 'false'}</span>
        </div>
      </div>
    </Link>
  )
}

export default Marker
