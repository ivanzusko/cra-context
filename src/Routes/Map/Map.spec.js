/* eslint-env jest */
import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter } from 'react-router-dom'
import GoogleMapReact from 'google-map-react'
import AppContext, { mockData } from '../../context'
import Map, { getColor, createShapesOnTheMap } from './index'

const mockIncident = {
  _id: '5ed60f01f82566b3a5363c4f',
  status: {
    criticality: 'critical',
    verified: false
  },
  address: '554 Kensington Street, Weedville, Idaho, 8448',
  position: {
    from: {
      latitude: 24.416233,
      longitude: -116.950522
    },
    to: {
      latitude: 53.557797,
      longitude: 168.480582
    }
  },
  interval: {
    startTime: '2014-11-06T03:10:20 -01:00',
    endTime: '2015-10-20T07:31:00 -02:00',
    entryTime: '2015-09-11T05:54:12 -02:00'
  }
}

describe('<Map />', () => {
  let wrapper
  let getWrapper
  let contextValues

  beforeEach(() => {
    contextValues = {
      incidents: [],
      selectedIncident: null,
      setSelectedIncident: jest.fn()
    }
    jest.spyOn(React, 'useEffect').mockImplementation(f => f())

    getWrapper = values => mount(
      <AppContext.Provider value={{ ...contextValues, ...values }}>
        <BrowserRouter>
          <Map />
        </BrowserRouter>
      </AppContext.Provider>
    )
  })

  it('renders without crashing', () => {
    wrapper = getWrapper()

    expect(wrapper.find(GoogleMapReact).length).toEqual(1)
  })

  describe('setSelectedIncident', () => {
    it('should select the first incident when there is no selected incident', () => {
      const setSelectedIncident = jest.fn()
      wrapper = getWrapper({ incidents: mockData, selectedIncident: null, setSelectedIncident })

      expect(setSelectedIncident).toHaveBeenCalledWith(mockData[0])
    })
    it('should not be called when there is selected incident', () => {
      const setSelectedIncident = jest.fn()
      wrapper = getWrapper({ incidents: mockData, selectedIncident: mockIncident, setSelectedIncident })

      expect(setSelectedIncident).toHaveBeenCalledTimes(0)
    })
  })

  it('getColor returns proper color', () => {
    const criticalityOptions = ['critical', 'warning', 'info']
    const colorOptions = ['red', 'orange', 'green']

    criticalityOptions.forEach((label, i) => {
      expect(getColor(label)).toEqual(colorOptions[i])
    })
  })

  it('createShapesOnTheMap', () => {
    const incidents = mockData
    const setSelectedIncident = jest.fn()
    const createPath = jest.fn().mockImplementation(() => ({
      setMap: jest.fn(),
      addListener: jest.fn()
    }))
    const google = jest.fn()

    createShapesOnTheMap(incidents, setSelectedIncident, createPath, google)
    expect(createShapesOnTheMap).toBeTruthy()
    expect(createPath).toHaveBeenCalledTimes(incidents.length)
  })
})
