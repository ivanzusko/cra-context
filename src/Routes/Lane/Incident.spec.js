/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Incident from './Incident'
import Card from '../../components/Card'

const mockIncident = {
  _id: '5ed60f01dac2bdb26a1bb747',
  status: {
    criticality: 'warning',
    verified: false
  },
  address: '876 Seabring Street, Baker, Louisiana, 4442',
  position: {
    from: {
      latitude: 75.509597,
      longitude: -97.203744
    },
    to: {
      latitude: -22.743405,
      longitude: 10.017638
    }
  },
  interval: {
    startTime: '2018-11-24T09:51:02 -01:00',
    endTime: '2017-12-19T07:57:41 -01:00',
    entryTime: '2016-09-04T10:22:01 -02:00'
  }
}

describe('<Incident> route', () => {
  const setSelectedIncident = jest.fn()
  const props = {
    incident: mockIncident,
    setSelectedIncident
  }
  const wrapper = shallow(<Incident {...props} />)

  it('renders without crashing', () => {
    expect(wrapper.find(Card).length).toEqual(1)
  })
  it('should select incident on mouse enter', () => {
    wrapper.find(Card).props().onMouseEnter()
    expect(setSelectedIncident).toBeCalled()
  })
})
