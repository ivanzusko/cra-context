import React from 'react'
import Map from '../Map'
import Sidebar from './Sidebar'
import Loader from '../../components/Loader'

import './Lane.css'

export default function Lane () {
  return (
    <div className='Lane'>
      <Loader />
      <div className='Lane-content'>
        <div className='Lane-map'>
          <Map />
        </div>
        <Sidebar />
      </div>
    </div>
  )
}
