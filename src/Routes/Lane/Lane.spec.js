/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Lane from './index'
import Map from '../Map'
import Sidebar from './Sidebar'

describe('<Lane> route', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Lane />)

    expect(wrapper.hasClass('Lane')).toBe(true)
    expect(wrapper.find(Map).length).toEqual(1)
    expect(wrapper.find(Sidebar).length).toEqual(1)
  })
})
