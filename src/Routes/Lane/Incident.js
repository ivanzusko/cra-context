import React from 'react'
import { Link } from 'react-router-dom'
import Card from '../../components/Card'
import CardTitle from '../../components/Card/CardTitle'
import CardContent from '../../components/Card/CardContent'
import CardFooter from '../../components/Card/CardFooter'
import CardSubTitle from '../../components/Card/CardSubTitle'
import ChipRow from '../../components/Chip/ChipRow'
import ChipCriticality from '../../components/Chip/ChipCriticality'
import ChipVerified from '../../components/Chip/ChipVerified'
import CardButton from '../../components/Card/CardButton'
import Space from '../../components/Space'

import './Lane.css'

function Incident ({ incident, setSelectedIncident }) {
  const { _id, address, status: { criticality, verified } } = incident

  return (
    <Space bottom size='l'>
      <Card onMouseEnter={() => setSelectedIncident(incident)} data-cy='incident-card'>
        <CardContent>
          <CardTitle>{address}</CardTitle>
          <CardSubTitle>{_id}</CardSubTitle>
          <ChipRow>
            <ChipCriticality criticality={criticality} />
            <ChipVerified verified={verified} />
          </ChipRow>
        </CardContent>
        <CardFooter align='right'>
          <Link to={`/incident/${_id}`} className='Lane-link' data-cy='to-details-link' data-cy-link-incident-id={_id}>
            <CardButton>Details</CardButton>
          </Link>
        </CardFooter>
      </Card>
    </Space>
  )
}

export default Incident
