/* eslint-env jest */
import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter } from 'react-router-dom'
import { act } from 'react-dom/test-utils'
import AppContext from '../../../context'
import Search, { getSearchValue, getFilteredList } from '../Search'
import Input from '../../../components/Input'

describe('<Sidebar>', () => {
  let wrapper
  let getWrapper
  let contextValues

  const mockFilteredIncidents = [{
    _id: '5ed60f01dac2bdb26a1bb747',
    status: {
      criticality: 'warning',
      verified: false
    },
    address: '876 Seabring Street, Baker, Louisiana, 4442',
    position: {
      from: {
        latitude: 75.509597,
        longitude: -97.203744
      },
      to: {
        latitude: -22.743405,
        longitude: 10.017638
      }
    },
    interval: {
      startTime: '2018-11-24T09:51:02 -01:00',
      endTime: '2017-12-19T07:57:41 -01:00',
      entryTime: '2016-09-04T10:22:01 -02:00'
    }
  },
  {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: '554 Kensington Street, Weedville, Idaho, 8448',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  },
  {
    _id: '5ed60f01d1c183b898c84a03',
    status: {
      criticality: 'warning',
      verified: true
    },
    address: '699 Jewel Street, Whitehaven, Iowa, 6558',
    position: {
      from: {
        latitude: 88.258495,
        longitude: 47.787136
      },
      to: {
        latitude: -89.426602,
        longitude: -157.049446
      }
    },
    interval: {
      startTime: '2016-02-01T09:27:23 -01:00',
      endTime: '2016-01-05T09:57:52 -01:00',
      entryTime: '2016-05-05T12:55:57 -02:00'
    }
  }]

  const mockIncident = {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: '554 Kensington Street, Weedville, Idaho, 8448',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  }

  beforeEach(() => {
    contextValues = {
      filteredIncidents: [],
      setSelectedIncident: jest.fn().mockResolvedValue(mockIncident)
    }
    jest.spyOn(React, 'useEffect').mockImplementation(f => f())

    getWrapper = values => mount(
      <AppContext.Provider value={{ ...contextValues, ...values }}>
        <BrowserRouter>
          <Search />
        </BrowserRouter>
      </AppContext.Provider>
    )
  })

  it('renders without crashes', () => {
    wrapper = getWrapper()

    expect(wrapper.find('.Search').length).toEqual(1)
  })
  it('change state with filterQuery', () => {
    const setInputValue = jest.fn()
    wrapper = getWrapper({ filterQuery: 'jo' })
    const handleChange = jest.spyOn(React, 'useState')
    handleChange.mockImplementation(inputValue => [inputValue, setInputValue])

    expect(setInputValue).toBeTruthy()
  })
  it('handleChange', () => {
    const setFilterQuery = jest.fn()
    const setFilteredIncidents = jest.fn()
    wrapper = getWrapper({ incidents: mockFilteredIncidents, setFilterQuery, setFilteredIncidents })

    act(() => {
      wrapper.find(Input).props().onChange('mock-name', 'mock-value')
    })
    expect(setFilterQuery).toBeCalled()
    expect(setFilteredIncidents).toBeCalled()
  })
  it('getSearchValue returns lowercased search query', () => {
    expect(getSearchValue('Mock-value')).toEqual('mock-value')
  })
  it('getFilteredList returns list properly filtered by address or _id', () => {
    expect(getFilteredList(mockFilteredIncidents, 'kens')).toEqual([{
      _id: '5ed60f01f82566b3a5363c4f',
      status: {
        criticality: 'critical',
        verified: false
      },
      address: '554 Kensington Street, Weedville, Idaho, 8448',
      position: {
        from: {
          latitude: 24.416233,
          longitude: -116.950522
        },
        to: {
          latitude: 53.557797,
          longitude: 168.480582
        }
      },
      interval: {
        startTime: '2014-11-06T03:10:20 -01:00',
        endTime: '2015-10-20T07:31:00 -02:00',
        entryTime: '2015-09-11T05:54:12 -02:00'
      }
    }])
    expect(getFilteredList(mockFilteredIncidents, '5ed60f01d')).toEqual([{
      _id: '5ed60f01dac2bdb26a1bb747',
      status: {
        criticality: 'warning',
        verified: false
      },
      address: '876 Seabring Street, Baker, Louisiana, 4442',
      position: {
        from: {
          latitude: 75.509597,
          longitude: -97.203744
        },
        to: {
          latitude: -22.743405,
          longitude: 10.017638
        }
      },
      interval: {
        startTime: '2018-11-24T09:51:02 -01:00',
        endTime: '2017-12-19T07:57:41 -01:00',
        entryTime: '2016-09-04T10:22:01 -02:00'
      }
    }, {
      _id: '5ed60f01d1c183b898c84a03',
      status: {
        criticality: 'warning',
        verified: true
      },
      address: '699 Jewel Street, Whitehaven, Iowa, 6558',
      position: {
        from: {
          latitude: 88.258495,
          longitude: 47.787136
        },
        to: {
          latitude: -89.426602,
          longitude: -157.049446
        }
      },
      interval: {
        startTime: '2016-02-01T09:27:23 -01:00',
        endTime: '2016-01-05T09:57:52 -01:00',
        entryTime: '2016-05-05T12:55:57 -02:00'
      }
    }])
  })
})
