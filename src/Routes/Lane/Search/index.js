import React, { useEffect, useState } from 'react'
import { useAppContext } from '../../../context'
import Input from '../../../components/Input'

import './Search.css'

function Search () {
  const appContext = useAppContext()
  const {
    incidents,
    setFilteredIncidents, filterQuery, setFilterQuery
  } = appContext
  const [inputValue, setInputValue] = useState('')

  useEffect(() => {
    if (filterQuery) {
      setInputValue(filterQuery)
    }
  }, [filterQuery])

  const handleChange = (_, value) => {
    const searchValue = getSearchValue(value)
    const arr = getFilteredList(incidents, searchValue)

    setInputValue(searchValue)
    setFilterQuery(searchValue)
    setFilteredIncidents(arr)
  }

  return (
    <div className='Search'>
      <Input
        type='search'
        label='search'
        name='search'
        value={inputValue}
        onChange={handleChange}
      />
    </div>
  )
}

export function getSearchValue (value) {
  return value.toLowerCase()
}
export function getFilteredList (incidents, searchValue) {
  const check = item => item.toLowerCase().indexOf(searchValue) !== -1
  const arr = incidents.filter(incident => {
    return check(incident.address) || check(incident._id)
  })

  return arr
}
export default Search
