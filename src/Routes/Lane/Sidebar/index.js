import React from 'react'
import { useAppContext } from '../../../context'
import Incident from '../Incident'
import Search from '../Search'

import './Sidebar.css'

function Sidebar () {
  const appContext = useAppContext()
  const {
    filteredIncidents,
    setSelectedIncident
  } = appContext

  return (
    <div className='Sidebar'>
      <Search />
      <div className='Sidebar-list'>
        {!filteredIncidents.length &&
          <p>No incidents</p>}
        {filteredIncidents.map((incident) => (
          <Incident
            key={incident._id}
            incident={incident}
            setSelectedIncident={setSelectedIncident}
          />
        ))}
      </div>
    </div>
  )
}

export default Sidebar
