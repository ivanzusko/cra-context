/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import { Route } from 'react-router'
import Routes from './index'
import Lane from './Lane'
import Incident from './Incident'

describe('<Routes /> component', () => {
  const wrapper = shallow(<Routes />)

  it('renders without crashing and contain all Routes', () => {
    expect(wrapper.find(Route).length).toBe(2)
  })

  it('renders without crashing and contain all Routes with correct pathes', () => {
    expect(wrapper.find(Route).get(0).props.component).toEqual(Lane)
    expect(wrapper.find(Route).get(1).props.component).toEqual(Incident)
    expect(wrapper.find(Route).get(0).props.path).toEqual('/')
    expect(wrapper.find(Route).get(1).props.path).toEqual('/incident/:_id')
  })
})
