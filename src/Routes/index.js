import React from 'react'
import { Route, Switch } from 'react-router'
import Lane from './Lane'
import Incident from './Incident'

function Routes () {
  return (
    <Switch>
      <Route path='/' exact component={Lane} />
      <Route path='/incident/:_id' component={Incident} />
    </Switch>
  )
}

export default Routes
