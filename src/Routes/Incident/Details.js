import React from 'react'
import assocPath from '@bit/ramda.ramda.assoc-path'
import { useAppContext } from '../../context'
import Paper from '../../components/Paper'
import CardTitle from '../../components/Card/CardTitle'
import CardSubTitle from '../../components/Card/CardSubTitle'
import Input from '../../components/Input'
import Space from '../../components/Space'
import RadioGroup from '../../components/RadioGroup'
import Select from '../../components/Select'

function Details () {
  const {
    selectedIncident,
    modifiedIncident, setModifiedIncident
  } = useAppContext()

  if (!selectedIncident) return <p>Loading...</p>

  const {
    _id,
    address,
    status: { criticality, verified },
    interval: { startTime, endTime, entryTime },
    position: { from, to }
  } = selectedIncident

  const handleChange = (name, value) => {
    const incident = getIncident(name, value, modifiedIncident, selectedIncident)

    setModifiedIncident(incident)
  }

  return (
    <div>
      <Space bottom size='m'>
        <Input label='id' type='text' name='_id' disabled value={_id} />
        <Input label='address' type='text' name='address' value={address} onChange={handleChange} />
      </Space>

      <Space bottom size='xl'>
        <Paper>
          <Space bottom size='s'>
            <CardTitle>Criticality</CardTitle>
          </Space>
          <Select
            value={criticality}
            label='criticality'
            name='status.criticality'
            onChange={handleChange}
            options={[
              {
                value: 'critical',
                label: 'Critical'
              },
              {
                value: 'warning',
                label: 'Warning'
              },
              {
                value: 'info',
                label: 'Info'
              }]}
          />
        </Paper>
      </Space>

      <Space bottom size='xl'>
        <Paper>
          <Space bottom size='s'>
            <CardTitle>Verified</CardTitle>
          </Space>
          <RadioGroup
            value={`${verified}`}
            name='status.verified'
            onChange={handleChange}
            options={[
              {
                value: 'true',
                label: 'true'
              },
              {
                value: 'false',
                label: 'false'
              }]}
          />
        </Paper>
      </Space>

      <Space bottom size='xl'>
        <Paper>
          <Space bottom size='s'>
            <CardTitle>Interval</CardTitle>
          </Space>
          <Input label='start time' type='text' name='interval.startTime' value={startTime} onChange={handleChange} />
          <Input label='end time' type='text' name='interval.endTime' value={endTime} onChange={handleChange} />
          <Input label='entry time' type='text' name='interval.entryTime' value={entryTime} onChange={handleChange} />
        </Paper>
      </Space>
      <Paper>
        <Space bottom size='s'>
          <CardTitle>Position</CardTitle>
        </Space>
        <CardSubTitle>From:</CardSubTitle>
        <Input label='latitude' type='text' name='position.from.latitude' value={from.latitude} onChange={handleChange} />
        <Input label='longitude' type='text' name='position.from.longitude' value={from.longitude} onChange={handleChange} />
        <CardSubTitle>To:</CardSubTitle>
        <Input label='latitude' type='text' name='position.to.latitude' value={to.latitude} onChange={handleChange} />
        <Input label='longitude' type='text' name='position.to.longitude' value={to.longitude} onChange={handleChange} />
      </Paper>
    </div>
  )
}

export function getIncident (name, value, modifiedIncident, selectedIncident) {
  const propertyPath = name.split('.')
  const currentIncident = modifiedIncident || selectedIncident

  return assocPath(propertyPath, value, currentIncident)
}

export default Details
