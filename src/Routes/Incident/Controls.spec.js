/* eslint-env jest */
import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter } from 'react-router-dom'
import AppContext from '../../context'
import Controls from './Controls'

import Button from '../../components/Button'

describe('<Controls>', () => {
  let wrapper
  let getWrapper

  const modifiedIncident = {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: 'Berlin, Deutschland',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  }

  beforeEach(() => {
    jest.spyOn(React, 'useEffect').mockImplementation(f => f())

    getWrapper = values => mount(
      <AppContext.Provider value={{ ...values }}>
        <BrowserRouter>
          <Controls />
        </BrowserRouter>
      </AppContext.Provider>
    )
  })

  it('Should render with anything if there is no modifiedIncident', () => {
    const values = {
      modifiedIncident: null
    }

    wrapper = getWrapper(values)

    expect(wrapper.find(Button).length).toEqual(1)
    expect(wrapper.find(Button).text()).toEqual('Return')
  })

  it('Should render with all controls when modifiedIncident', () => {
    const values = {
      modifiedIncident: modifiedIncident
    }

    wrapper = getWrapper(values)

    const controlsList = ['Discard', 'Save', 'Return']

    expect(wrapper.find(Button).length).toEqual(3)
    wrapper.find(Button).forEach((item, i) => {
      expect(item.text()).toEqual(controlsList[i])
    })
  })

  it('handleDiscard', () => {
    const setSelectedIncident = jest.fn()
    const setModifiedIncident = jest.fn()

    wrapper = getWrapper({ setSelectedIncident, modifiedIncident, setModifiedIncident })

    wrapper.find('[data-cy="btn-discard"]').at(0).simulate('click')
    expect(setSelectedIncident).toBeCalledWith(null)
    expect(setModifiedIncident).toBeCalledWith(null)
  })

  it('handleReturn', () => {
    const setSelectedIncident = jest.fn()
    const setModifiedIncident = jest.fn()

    wrapper = getWrapper({ setSelectedIncident, modifiedIncident, setModifiedIncident })
    wrapper.find('[data-cy="btn-return"]').at(0).simulate('click')
    expect(setModifiedIncident).toBeCalledWith(null)
  })
})
