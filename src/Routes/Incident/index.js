import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router'
import { useAppContext } from '../../context'
import Container from '../../components/Container'
import Paper from '../../components/Paper'
import Space from '../../components/Space'
import Snackbar from '../../components/Snackbar'
import Controls from './Controls'
import Details from './Details'

import './Incident.css'

function Incident () {
  const { _id } = useParams()
  const {
    incidents,
    selectedIncident, setSelectedIncident,
    modifiedIncident, setModifiedIncident,
    saveIncident
  } = useAppContext()
  const [isSaved, setIsSaved] = useState(false)

  // we need this in case user comes directly by incident url.
  // ideally it would be API call using the url _id parameter,
  // which would return the requested incident.
  const defaultIncident = findIncidentById(incidents, _id)

  useEffect(() => {
    if (!selectedIncident) {
      setSelectedIncident(defaultIncident)
    }
  }, [selectedIncident, defaultIncident, setSelectedIncident])

  const handleSubmit = (event) => {
    event.preventDefault()

    setSelectedIncident(modifiedIncident)
    saveIncident()
    // handleCallSomeEndpoin(modifiedIncident)
    setIsSaved(true)
    setModifiedIncident(null)
  }

  return (
    <form
      className='Incident'
      action=''
      method='post'
      onSubmit={handleSubmit}
      id='IncidentForm'
    >
      <Container>
        <Paper>
          <div className='Incident-header'>
            <h1 className='Incident-title'>Incident</h1>
            <Controls />
          </div>

          <Details />

          <Space top size='xl'>
            <Controls />
          </Space>
        </Paper>

        <Snackbar
          label='Saved'
          show={isSaved}
          handleClose={() => setIsSaved(false)}
        />
      </Container>
    </form>
  )
}

export default Incident

export function findIncidentById (data, id) {
  return data.find((incident) => incident._id === id)
}
