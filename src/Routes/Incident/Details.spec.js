/* eslint-env jest */
import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount } from 'enzyme'
import AppContext from '../../context'
import Details, { getIncident } from './Details'
import CardTitle from '../../components/Card/CardTitle'
import Select from '../../components/Select'

describe('<Details>', () => {
  let wrapper
  let getWrapper

  const mockIncident = {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: '554 Kensington Street, Weedville, Idaho, 8448',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  }
  const modifiedIncident = {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: 'Berlin, Deutschland',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  }

  beforeEach(() => {
    jest.spyOn(React, 'useEffect').mockImplementation(f => f())

    getWrapper = values => mount(
      <AppContext.Provider value={{ ...values }}>
        <Details />
      </AppContext.Provider>
    )
  })

  it('should show loading message if incident is not selected', () => {
    const values = {
      selectedIncident: null
    }

    const wrapper = getWrapper(values)

    expect(wrapper.find('p').text()).toBe('Loading...')
  })

  it('renders without crashing', () => {
    const values = {
      selectedIncident: mockIncident
    }
    const wrapper = getWrapper(values)

    expect(wrapper.find(CardTitle).length).toEqual(4)
  })

  it('handleChange', () => {
    const setModifiedIncident = jest.fn()
    wrapper = getWrapper({ selectedIncident: mockIncident, modifiedIncident: modifiedIncident, setModifiedIncident })

    act(() => {
      wrapper.find(Select).props().onChange('mock-name', 'mock-value')
    })
    expect(setModifiedIncident).toBeCalled()
  })

  it('getIncident if there is selected or modified incident', () => {
    expect(getIncident('status.criticality', 'info', mockIncident, modifiedIncident)).toEqual({
      _id: '5ed60f01f82566b3a5363c4f',
      status: {
        criticality: 'info',
        verified: false
      },
      address: '554 Kensington Street, Weedville, Idaho, 8448',
      position: {
        from: {
          latitude: 24.416233,
          longitude: -116.950522
        },
        to: {
          latitude: 53.557797,
          longitude: 168.480582
        }
      },
      interval: {
        startTime: '2014-11-06T03:10:20 -01:00',
        endTime: '2015-10-20T07:31:00 -02:00',
        entryTime: '2015-09-11T05:54:12 -02:00'
      }
    })
    expect(getIncident('status.criticality', 'info', null, modifiedIncident)).toEqual({
      _id: '5ed60f01f82566b3a5363c4f',
      status: {
        criticality: 'info',
        verified: false
      },
      address: 'Berlin, Deutschland',
      position: {
        from: {
          latitude: 24.416233,
          longitude: -116.950522
        },
        to: {
          latitude: 53.557797,
          longitude: 168.480582
        }
      },
      interval: {
        startTime: '2014-11-06T03:10:20 -01:00',
        endTime: '2015-10-20T07:31:00 -02:00',
        entryTime: '2015-09-11T05:54:12 -02:00'
      }
    })
  })
})
