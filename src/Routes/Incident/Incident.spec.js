/* eslint-env jest */
import React from 'react'
import routeData from 'react-router'
import { BrowserRouter } from 'react-router-dom'
import { mount } from 'enzyme'
import AppContext, { mockData } from '../../context'
import Incident, { findIncidentById } from './index'
import Controls from './Controls'
import Details from './Details'
import Snackbar from '../../components/Snackbar'

describe('<Incident> route', () => {
  let wrapper
  let getWrapper
  let contextValues = {}

  const event = { preventDefault: () => {} }
  const mockIncident = {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: '554 Kensington Street, Weedville, Idaho, 8448',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  }

  const modifiedIncident = {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: 'Berlin, Deutschland',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  }

  beforeEach(() => {
    contextValues = {
      incidents: [],
      setSelectedIncident: jest.fn().mockResolvedValue(mockIncident)
    }
    const mockLocation = {
      pathname: '/incident',
      hash: '',
      search: '',
      state: '',
      _id: 'mock-id'
    }
    jest.spyOn(routeData, 'useParams').mockReturnValue(mockLocation)
    jest.spyOn(React, 'useEffect').mockImplementation(f => f())

    getWrapper = values => mount(
      <AppContext.Provider value={{ ...contextValues, ...values }}>
        <BrowserRouter>
          <Incident />
        </BrowserRouter>
      </AppContext.Provider>
    )
  })

  it('findIncidentById function should return incident by id', () => {
    const id = '5ed60f01f82566b3a5363c4f'
    const expectedIncident = mockIncident

    expect(findIncidentById(mockData, id)).toStrictEqual(expectedIncident)
  })

  it('renders without crashing', () => {
    wrapper = getWrapper()

    expect(wrapper.find('form').length).toEqual(1)
    expect(wrapper.find('form').prop('id')).toEqual('IncidentForm')
    expect(wrapper.find('h1').text()).toBe('Incident')
    expect(wrapper.contains(Snackbar)).toBe(true)
    expect(wrapper.find(Controls).length).toBe(2)
    expect(wrapper.contains(Details)).toBe(true)
  })

  it('should set selectedIncident if it doesnt exists on initial render', () => {
    wrapper = getWrapper({ selectedIncident: null })

    expect(contextValues.setSelectedIncident).toHaveBeenCalledTimes(1)
  })

  it('should not call setSelectedIncident if there is selectedIncident', () => {
    wrapper = getWrapper({ selectedIncident: mockIncident })

    expect(contextValues.setSelectedIncident).toHaveBeenCalledTimes(0)
  })

  it('Snackbar > handleClose', () => {
    const setIsSaved = jest.fn()
    const handleClose = jest.spyOn(React, 'useState')
    handleClose.mockImplementation(isSaved => [isSaved, setIsSaved])

    wrapper = getWrapper()

    wrapper.find(Snackbar).props().handleClose()
    expect(setIsSaved).toBeTruthy()
  })

  it('should save the modified incident', () => {
    jest.spyOn(event, 'preventDefault')
    const setState = jest.fn()
    const useStateSpy = jest.spyOn(React, 'useState')
    useStateSpy.mockImplementation((init) => [init, setState])

    contextValues = {
      incidents: [],
      modifiedIncident,
      saveIncident: jest.fn(),
      setModifiedIncident: jest.fn(),
      setSelectedIncident: jest.fn().mockResolvedValue(mockIncident)
    }

    wrapper = getWrapper()

    wrapper.find('button[type="submit"]').at(0).simulate('submit')

    expect(event.preventDefault).not.toBeCalled()
    expect(contextValues.setSelectedIncident).toBeCalledWith(modifiedIncident)
    expect(contextValues.saveIncident).toBeCalled()
    expect(contextValues.setModifiedIncident).toBeCalledWith(null)
  })
})
