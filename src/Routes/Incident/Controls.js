import React from 'react'
import { Link } from 'react-router-dom'
import { useAppContext } from '../../context'
import ButtonGroup from '../../components/ButtonGroup'
import Button from '../../components/Button'

function Controls () {
  const {
    setSelectedIncident,
    modifiedIncident,
    setModifiedIncident
  } = useAppContext()

  const handleDiscard = () => {
    setModifiedIncident(null)
    setSelectedIncident(null)
  }

  const handleReturn = () => {
    setModifiedIncident(null)
  }

  return (
    <ButtonGroup>
      {modifiedIncident &&
        <>
          <Button secondary onClick={() => handleDiscard()} data-cy='btn-discard'>
            Discard
          </Button>

          <Button type='submit' form='IncidentForm' primary data-cy='btn-save'>
            Save
          </Button>
        </>}
      <Button onClick={() => handleReturn()} data-cy='btn-return'>
        <Link to='/'>Return</Link>
      </Button>
    </ButtonGroup>
  )
}

export default Controls
