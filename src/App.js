import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import Routes from './Routes'
import './styles.css'
import './normalize.css'

export default function App () {
  return (
    <BrowserRouter>
      <div className='App'>
        <Routes />
      </div>
    </BrowserRouter>
  )
}
