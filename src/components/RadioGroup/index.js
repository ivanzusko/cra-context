import React, { useState } from 'react'

import './RadioGroup.css'

function Radio (props) {
  const { options, value, name, onChange } = props
  const [selectedValue, setSelectedValue] = useState(value)

  const handleChange = e => {
    const { name, value } = e.target

    onChange(name, value === 'true')
    setSelectedValue(value)
  }

  return (
    <div role='radiogroup'>
      {
        options.map(option =>
          <label key={option.value} className='Radio' data-cy-radio-status={option.value}>
            <span className={`Radio-icon ${option.value === selectedValue && 'isChecked'}`}>
              <span className='Radio-icon__icon'>
                <input
                  className='Radio-icon__icon-input'
                  type='radio'
                  name={name}
                  value={option.value}
                  onChange={handleChange}
                />
                <div className='Radio-icon__icon-div'>
                  <svg className='Radio-icon__icon-svg1' focusable='false' viewBox='0 0 24 24' aria-hidden='true'><path d='M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.42 0-8-3.58-8-8s3.58-8 8-8 8 3.58 8 8-3.58 8-8 8z' /></svg>
                  <svg className='Radio-icon__icon-svg2' focusable='false' viewBox='0 0 24 24' aria-hidden='true'><path d='M8.465 8.465C9.37 7.56 10.62 7 12 7C14.76 7 17 9.24 17 12C17 13.38 16.44 14.63 15.535 15.535C14.63 16.44 13.38 17 12 17C9.24 17 7 14.76 7 12C7 10.62 7.56 9.37 8.465 8.465Z' /></svg>
                </div>
              </span>
              <span className='Radio-icon__touch' />
            </span>
            <span className='Radio-label'>{option.label}</span>
          </label>
        )
      }
    </div>
  )
}

export default Radio
