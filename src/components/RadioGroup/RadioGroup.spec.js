/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import RadioGroup from './index'

describe('<RadioGroup> component', () => {
  const getWrapper = props => shallow(<RadioGroup {...props} />)

  it('renders without crashing', () => {
    const props = {
      options: []
    }
    const wrapper = getWrapper(props)

    expect(wrapper.find('div').props().role).toEqual('radiogroup')
  })

  it('renders with options', () => {
    const props = {
      options: [
        {
          value: 'true',
          label: 'true'
        },
        {
          value: 'false',
          label: 'false'
        }
      ]
    }
    const wrapper = getWrapper(props)

    expect(wrapper.find('.Radio')).toHaveLength(2)
  })

  it('should handle onChange', () => {
    const handleChange = jest.fn()

    const inputProps = {
      label: 'mock-label',
      name: 'mock-name',
      value: 'siemens',
      options: [
        {
          value: 'true',
          label: 'true'
        },
        {
          value: 'false',
          label: 'false'
        }
      ],
      onChange: handleChange
    }
    const event = (value) => ({
      preventDefault () {},
      target: {
        name: 'mock-name',
        value
      }
    })
    const wrapper = getWrapper(inputProps)
    const inputs = wrapper.find('input').getElements()
    const [input1, input2] = inputs
    const simulateChange = (input) => [
      input.props.onChange(event(input.props.value))
    ]

    simulateChange(input1)
    expect(handleChange).toBeCalledWith(inputProps.name, true)
    expect(wrapper.find('span.isChecked').find('input').props().value).toEqual('true')
    simulateChange(input2)
    expect(handleChange).toBeCalledWith(inputProps.name, false)
    expect(wrapper.find('span.isChecked').find('input').props().value).toEqual('false')
  })
})
