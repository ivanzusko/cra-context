import React from 'react'

import './Space.css'

function Space (props) {
  const { children, bottom, top, size } = props

  return (
    <div
      className={`Space${top ? ' top' : ''}${bottom ? ' bottom' : ''} ${size} `}
    >
      {children}
    </div>
  )
}

export default Space
