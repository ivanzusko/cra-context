/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Space from './index'

describe('<Space> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Space />)
    expect(wrapper.hasClass('Space')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <Space>
        <div className='unique' />
        <div className='unique2' />
      </Space>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })

  it('renders with additional class names if respective props were passed', () => {
    const wrapper = shallow(<Space top bottom size='xxs' />)
    expect(wrapper.hasClass('Space top bottom xxs')).toEqual(true)
  })
})
