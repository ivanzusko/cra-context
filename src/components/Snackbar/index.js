import React, { useEffect } from 'react'

import './Snackbar.css'

function Snackbar (props) {
  const { label, show, handleClose } = props

  useEffect(() => {
    if (show) {
      const t = setTimeout(() => {
        handleClose()
      }, 3000)

      return () => {
        clearTimeout(t)
      }
    }
  }, [show, handleClose])

  if (!show) return null

  return (
    <div className='Snackbar' data-cy='snackbar'>
      <div className='Snackbar-icon'>
        <svg
          className='Snackbar-icon__svg'
          focusable='false'
          viewBox='0 0 24 24'
          aria-hidden='true'
        >
          <path d='M20,12A8,8 0 0,1 12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4C12.76,4 13.5,4.11 14.2, 4.31L15.77,2.74C14.61,2.26 13.34,2 12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0, 0 22,12M7.91,10.08L6.5,11.5L11,16L21,6L19.59,4.58L11,13.17L7.91,10.08Z' />
        </svg>
      </div>
      <div className='Snackbar-message'>{label}</div>
      <div className='Snackbar-action'>
        <button
          className='Snackbar-action__button'
          type='button'
          aria-label='Close'
          title='Close'
          onClick={handleClose}
        >
          <span className='Snackbar-action__icon'>
            <svg
              className='Snackbar-action__svg'
              focusable='false'
              viewBox='0 0 24 24'
              aria-hidden='true'
            >
              <path d='M19 6.41L17.59 5 12 10.59 6.41 5 5 6.41 10.59 12 5 17.59 6.41 19 12 13.41 17.59 19 19 17.59 13.41 12z' />
            </svg>
          </span>
        </button>
      </div>
    </div>
  )
}

export default Snackbar
