/* eslint-env jest */
import React from 'react'
import { mount, shallow } from 'enzyme'
import Snackbar from './index'

describe('<Snackbar> component', () => {
  const getWrapper = (props, show) => shallow(<Snackbar {...props} show={show} />)

  it('doesnt renders when `show` is not true', () => {
    const props = {
      label: 'mock-label',
      handleClose: jest.fn()
    }
    const wrapper = getWrapper(props, false)

    expect(wrapper.find('.Snackbar')).toHaveLength(0)
  })

  it('renders without crashing', () => {
    const props = {
      label: 'mock-label',
      handleClose: jest.fn()
    }
    const wrapper = getWrapper(props, true)

    expect(wrapper.find('.Snackbar')).toHaveLength(1)
    expect(wrapper.find('svg.Snackbar-icon__svg')).toHaveLength(1)
    expect(wrapper.find('.Snackbar-action__svg')).toHaveLength(1)
    expect(wrapper.find('.Snackbar-message').text()).toEqual(props.label)
    expect(wrapper.find('button.Snackbar-action__button')).toHaveLength(1)
    expect(wrapper.find('button.Snackbar-action__button').prop('aria-label')).toEqual('Close')
  })

  it('should handle close', () => {
    const props = {
      label: 'mock-label',
      handleClose: jest.fn()
    }
    const wrapper = mount(<Snackbar {...props} show />)

    expect(props.handleClose).not.toBeCalled()
    wrapper.find('button.Snackbar-action__button').simulate('click')
    expect(props.handleClose).toBeCalled()
    wrapper.setProps({ show: false })
    expect(wrapper.find('.Snackbar')).toHaveLength(0)
  })

  it('should handle close in useEffect', () => {
    jest.useFakeTimers()

    const props = {
      label: 'mock-label',
      handleClose: jest.fn()
    }
    mount(<Snackbar {...props} show />)

    expect(props.handleClose).not.toBeCalled()
    jest.advanceTimersByTime(2999)
    expect(props.handleClose).not.toBeCalled()
    jest.advanceTimersByTime(1)
    expect(props.handleClose).toBeCalled()
  })
})
