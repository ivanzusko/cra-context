/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import ButtonGroup from './index'

describe('<ButtonGroup> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<ButtonGroup />)
    expect(wrapper.hasClass('ButtonGroup')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <ButtonGroup>
        <div className='unique' />
        <div className='unique2' />
      </ButtonGroup>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
