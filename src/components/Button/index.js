import React from 'react'

import './Button.css'

function Button (props) {
  const { children, primary, secondary, disabled, type, onClick, ...other } = props

  return (
    <button
      className={
        `Button${primary ? ' Button--primary' : ''}${secondary ? ' Button--secondary' : ''}${disabled ? ' Button--disabled' : ''}`
      }
      type={type || 'button'}
      onClick={onClick}
      {...other}
    >
      {children}
    </button>
  )
}

export default Button
