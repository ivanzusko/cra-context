/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Button from './index'

describe('<Button> component', () => {
  it('renders without crashing', () => {
    const onClick = jest.fn()
    const wrapper = shallow(<Button onClick={onClick} />)

    expect(wrapper.hasClass('Button')).toEqual(true)
    expect(wrapper.props().type).toEqual('button')
    expect(wrapper.props()).toHaveProperty('onClick', onClick)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <Button>
        <span className='unique'>Button</span>
      </Button>
    ))
    expect(wrapper.find('span.unique').text()).toEqual('Button')
  })

  it('renders with additional class names if respective props were passed', () => {
    const wrapper = shallow((
      <Button primary secondary disabled />
    ))
    expect(wrapper.hasClass('Button--primary')).toEqual(true)
    expect(wrapper.hasClass('Button--secondary')).toEqual(true)
    expect(wrapper.hasClass('Button--disabled')).toEqual(true)
  })
})
