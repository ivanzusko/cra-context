/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Container from './index'

describe('<Container> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Container />)
    expect(wrapper.hasClass('Container')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <Container>
        <div className='unique' />
        <div className='unique2' />
      </Container>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
