/* eslint-env jest */
import React from 'react'
import { mount, shallow } from 'enzyme'
import Select from './index'

describe('<Select> component', () => {
  const handleChange = jest.fn()
  const props = {
    label: 'mock-label',
    name: 'mock-name',
    options: [
      {
        value: 'critical',
        label: 'Critical'
      },
      {
        value: 'warning',
        label: 'Warning'
      },
      {
        value: 'info',
        label: 'Info'
      }
    ],
    onChange: handleChange
  }
  const getWrapper = props => shallow(<Select {...props} />)

  it('renders without crashing', () => {
    const wrapper = getWrapper(props)

    expect(wrapper.find('.Input.Select')).toHaveLength(1)
    expect(wrapper.find('select.Select-select')).toHaveLength(1)
    expect(wrapper.find('select.Select-select').props().name).toEqual(props.name)
    expect(wrapper.find('label.Select-label').text()).toEqual(props.label)
    expect(wrapper.find('.Select-svg')).toHaveLength(1)
    expect(wrapper.find('fieldset.Select-outline')).toHaveLength(1)
    expect(wrapper.find('legend.Select-legend').find('span').text()).toEqual(props.label)
  })

  it('renders with passed value as initial value (through useEffect hook)', () => {
    const wrapper = mount(<Select {...props} value='Siemens' />)

    expect(wrapper.find('select').props().value).toEqual('Siemens')
  })

  it('renders with no value if nothig was passed (check in useEffect hook)', () => {
    const wrapper = mount(<Select {...props} value='' />)

    expect(wrapper.find('select').props().value).toEqual('')
  })

  it('should handle onChange', () => {
    const wrapper = getWrapper(props)

    const event = (value) => ({
      preventDefault () {},
      target: {
        name: 'mock-name',
        value
      }
    })

    wrapper.find('select').simulate('change', event('the-value'))
    expect(handleChange).toBeCalledWith(props.name, 'the-value')
    expect(wrapper.find('select').props().value).toBe('the-value')
  })
})
