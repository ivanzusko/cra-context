import React, { useState, useEffect } from 'react'

import './Select.css'

function Select (props) {
  const { label, value, name, options, onChange } = props
  const [filled, setFilled] = useState(false)
  const [inputValue, setInputValue] = useState('')

  useEffect(() => {
    if (value) {
      setInputValue(value)
    }
  }, [value])

  useEffect(() => {
    if (inputValue) {
      setFilled(true)
    } else {
      setFilled(false)
    }
  }, [inputValue])

  const handleChange = e => {
    const { name, value } = e.target

    onChange(name, value)
    setInputValue(value)
  }

  return (
    <div className={`Input Select ${filled && 'Input--filled Select--filled'}`}>
      <label className='Input-label Select-label'>{label}</label>
      <div className='Input-wrapper Select-wrapper'>
        <select
          className='Select-select'
          name={name}
          onChange={handleChange}
          value={inputValue}
        >
          {options.map(option => (
            <option key={option.value} value={option.value}>{option.label}</option>
          ))}
        </select>
        <svg
          className='Select-svg'
          focusable='false'
          viewBox='0 0 24 24'
          aria-hidden='true'
        >
          <path d='M7 10l5 5 5-5z' />
        </svg>
        <fieldset aria-hidden='true' className='Input-outline Select-outline'>
          <legend className='Input-legend Select-legend'>
            <span>{label}</span>
          </legend>
        </fieldset>
      </div>
    </div>
  )
}

export default Select
