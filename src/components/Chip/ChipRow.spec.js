/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import ChipRow from './ChipRow'

describe('<ChipRow> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<ChipRow />)
    expect(wrapper.hasClass('Chip-row')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <ChipRow>
        <div className='unique' />
        <div className='unique2' />
      </ChipRow>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
