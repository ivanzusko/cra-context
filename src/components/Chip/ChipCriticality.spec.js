/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import ChipCriticality from './ChipCriticality'

describe('<ChipCriticality> component', () => {
  const getWrapper = props => shallow(<ChipCriticality {...props} />)

  it('renders with Chip--info modifier and proper label', () => {
    const wrapper = getWrapper({ criticality: 'info' })

    expect(wrapper.hasClass('Chip--info')).toBe(true)
    expect(wrapper.find('span.Chip-label').text()).toEqual('info')
  })

  it('renders with Chip--warning modifier and proper label', () => {
    const wrapper = getWrapper({ criticality: 'warning' })

    expect(wrapper.hasClass('Chip--warning')).toBe(true)
    expect(wrapper.find('span.Chip-label').text()).toEqual('warning')
  })

  it('renders with Chip--critical modifier and proper label', () => {
    const wrapper = getWrapper({ criticality: 'critical' })

    expect(wrapper.hasClass('Chip--critical')).toBe(true)
    expect(wrapper.find('span.Chip-label').text()).toEqual('critical')
  })
})
