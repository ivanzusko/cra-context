import React from 'react'

import './Chip.css'

function ChipRow ({ children }) {
  return (
    <div className='Chip-row'>
      {children}
    </div>
  )
}

export default ChipRow
