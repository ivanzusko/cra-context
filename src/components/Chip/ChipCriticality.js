import React from 'react'

import './Chip.css'

function ChipCriticality ({ criticality }) {
  return (
    <div className={`Chip Chip--${criticality}`}>
      <span className='Chip-label'>{criticality}</span>
    </div>
  )
}

export default ChipCriticality
