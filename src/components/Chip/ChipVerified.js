import React from 'react'

import './Chip.css'

function Chip ({ verified }) {
  return (
    <div className={`Chip Chip--${verified ? 'verified' : 'notverified'}`}>
      <span className='Chip-label'>
        {verified ? 'verified' : 'not verified'}
      </span>
      {verified && (
        <svg
          className='Chip-icon'
          focusable='false'
          viewBox='0 0 24 24'
          aria-hidden='true'
        >
          <path d='M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4L9 16.2z' />
        </svg>
      )}
    </div>
  )
}

export default Chip
