/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import ChipVerified from './ChipVerified'

describe('<ChipVerified> component', () => {
  const getWrapper = props => shallow(<ChipVerified {...props} />)

  it('renders without crashing', () => {
    const wrapper = getWrapper()
    expect(wrapper.hasClass('Chip')).toEqual(true)
  })

  it('renders with Chip--verified modificator and proper content when verified prop was passed', () => {
    const wrapper = getWrapper({ verified: true })

    expect(wrapper.hasClass('Chip--verified')).toEqual(true)
    expect(wrapper.find('span.Chip-label').text()).toEqual('verified')
    expect(wrapper.find('svg.Chip-icon')).toHaveLength(1)
  })

  it('renders with Chip--notverified modificator and proper content when verified prop was passed', () => {
    const wrapper = getWrapper({ verified: false })

    expect(wrapper.hasClass('Chip--notverified')).toEqual(true)
    expect(wrapper.find('span.Chip-label').text()).toEqual('not verified')
    expect(wrapper.find('svg.Chip-icon')).toHaveLength(0)
  })
})
