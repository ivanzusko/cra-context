/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import CardSubTitle from './CardSubTitle'

describe('<CardSubTitle> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<CardSubTitle />)
    expect(wrapper.hasClass('Card-subtitle')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <CardSubTitle>
        <div className='unique' />
        <div className='unique2' />
      </CardSubTitle>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
