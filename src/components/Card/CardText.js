import React from 'react'

import './Card.css'

function CardTitle ({ children }) {
  return <p className='Card-p'>{children}</p>
}

export default CardTitle
