/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import CardFooter from './CardFooter'

describe('<CardFooter> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<CardFooter />)
    expect(wrapper.hasClass('Card-footer')).toEqual(true)
  })

  it('renders with additional alignment class if prop was passed', () => {
    const wrapper = shallow(<CardFooter align='right' />)
    expect(wrapper.hasClass('right')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <CardFooter>
        <div className='unique' />
        <div className='unique2' />
      </CardFooter>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
