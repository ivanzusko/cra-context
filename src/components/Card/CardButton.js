import React from 'react'

import './Card.css'

function CardButton ({ children }) {
  return (
    <button className='Card-button' type='button' data-cy='to-details'>
      {children}
    </button>
  )
}

export default CardButton
