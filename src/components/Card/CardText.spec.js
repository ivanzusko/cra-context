/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import CardText from './CardText'

describe('<CardText> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<CardText />)
    expect(wrapper.hasClass('Card-p')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <CardText>
        <div className='unique' />
        <div className='unique2' />
      </CardText>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
