import React from 'react'

import './Card.css'

function CardFooter ({ children, align }) {
  return <div className={`Card-footer${align ? ' ' + align : ''}`}>{children}</div>
}

export default CardFooter
