import React from 'react'

import './Card.css'

function CardTitle ({ children }) {
  return <div className='Card-content'>{children}</div>
}

export default CardTitle
