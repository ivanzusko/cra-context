import React from 'react'

import './Card.css'

function CardSubTitle ({ children }) {
  return (
    <h6 className='Card-subtitle'>{children}</h6>
  )
}

export default CardSubTitle
