/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Card from './index'

describe('<Card> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Card />)
    expect(wrapper.hasClass('Card')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <Card>
        <div className='unique' />
        <div className='unique2' />
      </Card>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })

  it('renders with all passed props', () => {
    const mockProps = {
      john: 'snow',
      bastard: false,
      knowsNothing: true
    }
    const wrapper = shallow((
      <Card {...mockProps} />
    ))

    expect(wrapper.props().john).toEqual('snow')
    expect(wrapper.props().bastard).toEqual(false)
    expect(wrapper.props().knowsNothing).toEqual(true)
  })
})
