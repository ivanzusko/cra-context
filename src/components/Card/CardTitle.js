import React from 'react'

import './Card.css'

function CardTitle ({ children }) {
  return (
    <h5 className='Card-title'>{children}</h5>
  )
}

export default CardTitle
