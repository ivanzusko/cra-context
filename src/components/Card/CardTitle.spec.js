/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import CardTitle from './CardTitle'

describe('<CardTitle> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<CardTitle />)
    expect(wrapper.hasClass('Card-title')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <CardTitle>
        <div className='unique' />
        <div className='unique2' />
      </CardTitle>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
