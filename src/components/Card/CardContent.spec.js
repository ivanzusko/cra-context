/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import CardContent from './CardContent'

describe('<CardContent> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<CardContent />)
    expect(wrapper.hasClass('Card-content')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <CardContent>
        <div className='unique' />
        <div className='unique2' />
      </CardContent>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
