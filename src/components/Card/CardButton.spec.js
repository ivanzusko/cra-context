/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import CardButton from './CardButton'

describe('<CardButton> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<CardButton />)
    expect(wrapper.hasClass('Card-button')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <CardButton>
        <div className='unique' />
        <div className='unique2' />
      </CardButton>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
