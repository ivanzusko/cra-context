/* eslint-env jest */
import React from 'react'
import { mount } from 'enzyme'
import { BrowserRouter } from 'react-router-dom'
import AppContext from '../../context'
import Loader from './index'

describe('<Loader> component', () => {
  let getWrapper
  let wrapper
  const contextValues = {
    isFetching: false
  }
  beforeEach(() => {
    getWrapper = values => mount(
      <AppContext.Provider value={{ ...contextValues, ...values }}>
        <BrowserRouter>
          <Loader />
        </BrowserRouter>
      </AppContext.Provider>
    )
  })

  it('renders without crashing when incidents are being fetched', () => {
    wrapper = getWrapper({ isFetching: true })

    expect(wrapper.find('.Loader')).toHaveLength(1)
  })

  it('should not render when fetching is finished', () => {
    wrapper = getWrapper({ isFetching: false })

    expect(wrapper.find('.Loader')).toHaveLength(0)
  })
})
