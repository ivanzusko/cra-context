import React from 'react'
import { useAppContext } from '../../context'
import './Loader.css'

function Loader () {
  const {
    isFetching
  } = useAppContext()

  if (!isFetching) return null

  return (
    <div className='Loader'><span>LOADING ...</span></div>
  )
}

export default Loader
