import React, { useState, useEffect } from 'react'

import './Input.css'

function Input (props) {
  const { label, name, type, disabled, value, onChange } = props
  const [focused, setFocused] = useState(false)
  const [filled, setFilled] = useState(false)
  const [inputValue, setInputValue] = useState('')

  useEffect(() => {
    if (value) {
      setInputValue(value)
    }
  }, [value])

  useEffect(() => {
    if (inputValue) {
      setFilled(true)
    } else {
      setFilled(false)
    }
  }, [inputValue])

  const handleChange = e => {
    const { name, value } = e.target

    onChange(name, value)
    setInputValue(value)
  }

  return (
    <div className={
      `Input${filled ? ' Input--filled' : ''}${focused ? ' Input--focused' : ''}${disabled ? ' Input--disabled' : ''}`
    }
    >
      <label className='Input-label'>{label}</label>
      <div className='Input-wrapper'>
        <input
          className='Input-input'
          name={name}
          type={type}
          onChange={handleChange}
          onFocus={() => {
            setFocused(true)
          }}
          onBlur={() => {
            setFocused(false)
          }}
          disabled={disabled}
          value={inputValue}
        />
        <fieldset
          aria-hidden='true'
          className='Input-outline'
        >
          <legend className='Input-legend'>
            <span>{label}</span>
          </legend>
        </fieldset>
      </div>
    </div>
  )
}

export default Input
