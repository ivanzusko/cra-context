/* eslint-env jest */
import React from 'react'
import { mount, shallow } from 'enzyme'
import Input from './index'

describe('<Input> component', () => {
  const getWrapper = props => shallow(<Input {...props} />)

  it('renders without crashing', () => {
    const wrapper = getWrapper()

    expect(wrapper.hasClass('Input')).toBe(true)
    expect(wrapper.find('fieldset')).toHaveLength(1)
    expect(wrapper.find('label.Input-label')).toHaveLength(1)
  })

  it('renders with props', () => {
    const props = {
      label: 'mock-label',
      name: 'mock-name',
      type: 'text',
      value: 'siemens',
      onChange: jest.fn()
    }
    const wrapper = shallow(<Input {...props} />)

    expect(wrapper.find('label.Input-label').text()).toEqual(props.label)
    expect(wrapper.find('input').props().name).toEqual(props.name)
    expect(wrapper.find('input').props().type).toEqual(props.type)
    expect(wrapper.find('span').text()).toEqual(props.label)
  })

  it('renders with passed value as initial value (through useEffect hook)', () => {
    const wrapper = mount(<Input value='Siemens' />)

    expect(wrapper.find('input').props().value).toEqual('Siemens')
  })

  it('renders with no value if nothig was passed (check in useEffect hook)', () => {
    const wrapper = mount(<Input value='' />)

    expect(wrapper.find('input').props().value).toEqual('')
  })

  it('renders as disabled', () => {
    const props = {
      disabled: true
    }
    const wrapper = getWrapper(props)

    expect(wrapper.hasClass('Input--disabled')).toBe(true)
    expect(wrapper.find('input').props().disabled).toEqual(true)
  })

  it('should handle onChange', () => {
    const handleChange = jest.fn()

    const props = {
      label: 'mock-label',
      name: 'mock-name',
      type: 'text',
      value: 'siemens',
      onChange: handleChange
    }
    const event = (value) => ({
      preventDefault () {},
      target: {
        name: 'mock-name',
        value
      }
    })
    const wrapper = getWrapper(props)

    wrapper.find('input').simulate('change', event('the-value'))
    expect(handleChange).toBeCalledWith(props.name, 'the-value')
    expect(wrapper.find('input').props().value).toBe('the-value')
    wrapper.find('input').simulate('change', event('another-value'))
    expect(wrapper.find('input').props().value).toBe('another-value')
  })

  it('should handle onFocus and onBlur', () => {
    const wrapper = getWrapper()

    expect(wrapper.hasClass('Input--focused')).toBe(false)
    wrapper.find('input').props().onFocus()
    expect(wrapper.hasClass('Input--focused')).toBe(true)
    wrapper.find('input').props().onBlur()
    expect(wrapper.hasClass('Input--focused')).toBe(false)
  })
})
