/* eslint-env jest */
import React from 'react'
import { shallow } from 'enzyme'
import Paper from './index'

describe('<Paper> component', () => {
  it('renders without crashing', () => {
    const wrapper = shallow(<Paper />)
    expect(wrapper.hasClass('Paper')).toEqual(true)
  })

  it('renders children when passed in', () => {
    const wrapper = shallow((
      <Paper>
        <div className='unique' />
        <div className='unique2' />
      </Paper>
    ))
    expect(wrapper.contains(<div className='unique' />)).toEqual(true)
    expect(wrapper.contains(<div className='unique2' />)).toEqual(true)
  })
})
