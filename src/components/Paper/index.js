import React from 'react'

import './Paper.css'

function Paper ({ children }) {
  return <div className='Paper'>{children}</div>
}

export default Paper
