import React, { createContext, useEffect, useState, useContext } from 'react'
const AppContext = createContext()

export const mockData = require('../data.json')

export function AppProvider (props) {
  const [needFetch, setNeedFetch] = useState(true)
  const [isFetching, setIsFetching] = useState(false)
  const [hasIncidentsFetchError, setHasIncidentsFetchError] = useState(false)
  const [incidents, setIncidents] = useState([])
  const [incident, setIncident] = useState(null)
  const [modifiedIncident, setModifiedIncident] = useState(null)
  const [filteredIncidents, setFilteredIncidents] = useState([])
  const [filterQuery, setFilterQuery] = useState('')

  useEffect(() => {
    if (needFetch) {
      setIsFetching(true)
      fetchIncidents(setHasIncidentsFetchError)
        // using mock data instead of placeholder API
        .then(data => {
          setIncidents(mockData)
          setNeedFetch(false)
          setIsFetching(false)
        })
    }
  }, [needFetch])

  useEffect(() => {
    setFilteredIncidents(incidents)
  }, [incidents])

  const handleSaveModifiedIncident = () => {
    const updatedIncidents = getUpdatedIncidents(incidents, modifiedIncident)

    setIncidents(updatedIncidents)
  }

  return (
    <AppContext.Provider value={{
      needFetch,
      hasIncidentsFetchError,
      incidents,
      selectedIncident: incident,
      setSelectedIncident: (incident) => {
        setIncident(incident)
      },
      modifiedIncident,
      setModifiedIncident: (incident) => {
        setModifiedIncident(incident)
      },
      saveIncident: handleSaveModifiedIncident,
      filteredIncidents,
      setFilteredIncidents: (incidents) => {
        setFilteredIncidents(incidents)
      },
      filterQuery,
      setFilterQuery,
      fetchIncidents,
      isFetching
    }}
    >
      {props.children}
    </AppContext.Provider>
  )
}

export async function fetchIncidents (errorHandler) {
  try {
    // @TODO: we will use proper endpoint
    const res = await window.fetch('https://jsonplaceholder.typicode.com/users')
    const responseData = await res.json()

    return responseData
  } catch (err) {
    errorHandler(err)
    return err
  }
}

export function getUpdatedIncidents (incidents, modifiedIncident) {
  const updatedIncidents = incidents.map(incident => {
    if (modifiedIncident._id === incident._id) {
      return modifiedIncident
    }
    return incident
  })

  return updatedIncidents
}
export const useAppContext = () => useContext(AppContext)

export default AppContext
