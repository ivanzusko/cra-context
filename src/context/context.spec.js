/* eslint-env jest */
import React from 'react'
import { act } from 'react-dom/test-utils'
import { mount, shallow } from 'enzyme'
import {
  AppProvider,
  getUpdatedIncidents,
  fetchIncidents
} from './index'

const smallMock = [
  {
    _id: '5ed60f01dac2bdb26a1bb747',
    status: {
      criticality: 'warning',
      verified: false
    },
    address: '876 Seabring Street, Baker, Louisiana, 4442',
    position: {
      from: {
        latitude: 75.509597,
        longitude: -97.203744
      },
      to: {
        latitude: -22.743405,
        longitude: 10.017638
      }
    },
    interval: {
      startTime: '2018-11-24T09:51:02 -01:00',
      endTime: '2017-12-19T07:57:41 -01:00',
      entryTime: '2016-09-04T10:22:01 -02:00'
    }
  },
  {
    _id: '5ed60f01f82566b3a5363c4f',
    status: {
      criticality: 'critical',
      verified: false
    },
    address: '554 Kensington Street, Weedville, Idaho, 8448',
    position: {
      from: {
        latitude: 24.416233,
        longitude: -116.950522
      },
      to: {
        latitude: 53.557797,
        longitude: 168.480582
      }
    },
    interval: {
      startTime: '2014-11-06T03:10:20 -01:00',
      endTime: '2015-10-20T07:31:00 -02:00',
      entryTime: '2015-09-11T05:54:12 -02:00'
    }
  }
]
global.fetch = jest.fn(() =>
  Promise.resolve({
    json: () => Promise.resolve(smallMock)
  })
)

describe('App context', () => {
  const setHasIncidentsFetchError = jest.fn()
  beforeEach(() => {
    global.fetch.mockClear()
  })

  describe('fetchIncidents', () => {
    it('returns incidents', async () => {
      const response = await fetchIncidents(setHasIncidentsFetchError)

      expect(response).toEqual(smallMock)
      expect(global.fetch).toHaveBeenCalledTimes(1)
    })
    it('returns exception', async () => {
      global.fetch.mockImplementationOnce(() =>
        Promise.reject(new Error('You know nothing, Jon Snow'))
      )

      const response = await fetchIncidents(setHasIncidentsFetchError)

      expect(response).toEqual(Error('You know nothing, Jon Snow'))
      expect(global.fetch).toHaveBeenCalledWith(
        'https://jsonplaceholder.typicode.com/users'
      )
    })
  })

  it('should pass the context correctly', () => {
    const handleSaveModifiedIncident = () => jest.fn()
    const provider = shallow(<AppProvider />)
    const spy = jest.spyOn(provider.props().value, 'setFilterQuery')
    const setFilterQuery = spy

    expect(JSON.stringify(provider.props().value)).toEqual(
      JSON.stringify({
        needFetch: true,
        hasIncidentsFetchError: false,
        incidents: [],
        selectedIncident: null,
        setSelectedIncident: jest.fn(),
        modifiedIncident: null,
        setModifiedIncident: jest.fn(),
        saveIncident: handleSaveModifiedIncident,
        filteredIncidents: [],
        setFilteredIncidents: () => jest.fn(),
        filterQuery: '',
        setFilterQuery,
        isFetching: false
      })
    )
  })

  it('should update the context', async () => {
    const provider = mount(
      <AppProvider
        value={{
          incidents: ['1', '2']
        }}
      />
    )
    const promise = () => {
      return new Promise(resolve => {
        setTimeout(() => {
          provider.update()
          resolve(provider)
        }, 300)
      })
    }

    await act(async () => {
      return promise().then((res) => {
        expect(res.props().value.incidents).toEqual(['1', '2'])
      })
    })
  })

  it('saveIncident', () => {
    const provider = shallow(<AppProvider />)
    const spy = jest.spyOn(provider.props().value, 'saveIncident')

    provider.props().value.saveIncident()
    expect(spy).toHaveBeenCalled()
  })

  it('setFilteredIncidents', () => {
    const provider = shallow(<AppProvider />)
    const spy = jest.spyOn(provider.props().value, 'setFilteredIncidents')

    provider.props().value.setFilteredIncidents()
    expect(spy).toHaveBeenCalled()
  })

  it('setSelectedIncident', () => {
    const provider = shallow(<AppProvider />)
    const spy = jest.spyOn(provider.props().value, 'setSelectedIncident')

    provider.props().value.setSelectedIncident()
    expect(spy).toHaveBeenCalled()
  })

  it('setModifiedIncident', () => {
    const provider = shallow(<AppProvider />)
    const spy = jest.spyOn(provider.props().value, 'setModifiedIncident')

    provider.props().value.setModifiedIncident()
    expect(spy).toHaveBeenCalled()
  })

  it('getUpdatedIncidents', () => {
    const incidents = [
      {
        _id: '5ed60f01f82566b3a5363c4f',
        status: {
          criticality: 'critical',
          verified: false
        },
        address: '554 Kensington Street, Weedville, Idaho, 8448',
        position: {
          from: {
            latitude: 24.416233,
            longitude: -116.950522
          },
          to: {
            latitude: 53.557797,
            longitude: 168.480582
          }
        },
        interval: {
          startTime: '2014-11-06T03:10:20 -01:00',
          endTime: '2015-10-20T07:31:00 -02:00',
          entryTime: '2015-09-11T05:54:12 -02:00'
        }
      },
      {
        _id: '5ed60f01d1c183b898c84a03',
        status: {
          criticality: 'warning',
          verified: true
        },
        address: '699 Jewel Street, Whitehaven, Iowa, 6558',
        position: {
          from: {
            latitude: 88.258495,
            longitude: 47.787136
          },
          to: {
            latitude: -89.426602,
            longitude: -157.049446
          }
        },
        interval: {
          startTime: '2016-02-01T09:27:23 -01:00',
          endTime: '2016-01-05T09:57:52 -01:00',
          entryTime: '2016-05-05T12:55:57 -02:00'
        }
      }
    ]
    const modifiedIncident = {
      _id: '5ed60f01d1c183b898c84a03',
      status: {
        criticality: 'warning',
        verified: true
      },
      address: 'Alexanderplatz, Berlin, Deutschland',
      position: {
        from: {
          latitude: 88.258495,
          longitude: 47.787136
        },
        to: {
          latitude: -89.426602,
          longitude: -157.049446
        }
      },
      interval: {
        startTime: '2016-02-01T09:27:23 -01:00',
        endTime: '2016-01-05T09:57:52 -01:00',
        entryTime: '2016-05-05T12:55:57 -02:00'
      }
    }

    expect(getUpdatedIncidents(incidents, modifiedIncident)).toEqual([
      {
        _id: '5ed60f01f82566b3a5363c4f',
        status: {
          criticality: 'critical',
          verified: false
        },
        address: '554 Kensington Street, Weedville, Idaho, 8448',
        position: {
          from: {
            latitude: 24.416233,
            longitude: -116.950522
          },
          to: {
            latitude: 53.557797,
            longitude: 168.480582
          }
        },
        interval: {
          startTime: '2014-11-06T03:10:20 -01:00',
          endTime: '2015-10-20T07:31:00 -02:00',
          entryTime: '2015-09-11T05:54:12 -02:00'
        }
      },
      {
        _id: '5ed60f01d1c183b898c84a03',
        status: {
          criticality: 'warning',
          verified: true
        },
        address: 'Alexanderplatz, Berlin, Deutschland',
        position: {
          from: {
            latitude: 88.258495,
            longitude: 47.787136
          },
          to: {
            latitude: -89.426602,
            longitude: -157.049446
          }
        },
        interval: {
          startTime: '2016-02-01T09:27:23 -01:00',
          endTime: '2016-01-05T09:57:52 -01:00',
          entryTime: '2016-05-05T12:55:57 -02:00'
        }
      }
    ])
  })
})
