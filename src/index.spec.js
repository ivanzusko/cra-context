/* eslint-env jest */
import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import { rootElement } from './index'

jest.mock('react-dom', () => ({ render: jest.fn() }))

describe('index.js', () => {
  it('renders without crashing', () => {
    ReactDOM.render(<App />, rootElement)
    expect(ReactDOM.render).toHaveBeenCalledWith(<App />, rootElement)
  })
})
