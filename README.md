# CRA Context


## Working app

App hosted by [Netlify](https://www.netlify.com/): [https://ivanzusko-s.netlify.app](https://ivanzusko-s.netlify.app)


## Instalation for development

`git clone https://github.com/ivanzusko/cra-context.git`

`cd cra-context`

`npm i`


## Commands

To start the local server with running app: `npm start` (Open [http://localhost:3000](http://localhost:3000) to view in the browser).

To run tests in watch mode: `npm test`

To run tests with coverage: `npm test -- --coverage --watchAll=false`

To lint the project: `npm run lint`

To deploy application: `npm run deploy:prod`


