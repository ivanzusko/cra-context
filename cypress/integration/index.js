import { inputValue } from '../utils'

describe('Home page', () => {
  describe('/', () => {
    describe('Everything rendered', () => {
      beforeEach(() => {
        cy.visit('http://localhost:3000')
      })

      it('List rendered with all incidents', () => {
        cy.get('[data-cy=to-details-link]').should('have.length', 400)
      })

      it('Searchbar rendered', () => {
        inputValue('input[name="search"]').is('')
      })

      it('Map rendered with Marker', () => {
        cy.get('[data-cy=map-marker]')
      })
    })

    describe('Map interactions', () => {
      it("By clicking the incident's marker it redirects user to incident details", () => {
        cy.visit('http://localhost:3000')

        cy.get('[data-cy=map-marker]').within(() => {
          cy
            .root()
            .should('have.attr', 'href')
            .then($href => {
              cy.log('href: ', $href)
              cy.root().click({ force: true })
              cy.location().should((loc) => {
                expect(loc.pathname).to.include($href)
              })
            })
        })
      })
    })

    describe('Sidebar interactions', () => {
      beforeEach(() => {
        cy.visit('http://localhost:3000')
      })

      describe('List interactions', () => {
        it('Hover over the item in sidebar shows the marker on the map', () => {
          cy.get('[data-cy=incident-card').eq(1).within(() => {
            cy.root().trigger('mouseover')
            cy.get('[data-cy=to-details-link]')
              .should('have.attr', 'data-cy-link-incident-id')
              .then($id => {
                cy.get('[data-cy-marker=marker-id]', { withinSubject: null }).contains($id)
              })
          })
        })

        it('Click on "Details" should redirect to incident page', () => {
          cy.get('[data-cy=incident-card]').eq(1).within(() => {
            cy.get('[data-cy=to-details-link]').should('have.attr', 'href')
              .then($href => {
                cy.visit($href)
              })
          })
        })
      })

      describe('Search bar interactions', () => {
        it('input to search should filter the list of incidents', () => {
          inputValue('input[name="search"]').change('du')
          cy.get('[data-cy=incident-card').should('have.length', 4)
          inputValue('input[name="search"]').edit('n')
          cy.get('[data-cy=incident-card').should('have.length', 2)
          inputValue('input[name="search"]').clear()
          cy.get('[data-cy=incident-card').should('have.length', 400)
        })
      })
    })
  })
})
