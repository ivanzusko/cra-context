import { inputValue } from '../utils'

describe('Incident page', () => {
  describe('"/incident/:_id"', () => {
    beforeEach(() => {
      cy.visit('http://localhost:3000/incident/5ed60f01dac2bdb26a1bb747')
    })

    it('Check address input and first Save button', () => {
      inputValue('input[name=address]').change('you know nothing, Jon Snow...')

      // - check if first Save works
      cy.get('[data-cy=btn-save]').should(($btn) => {
        $btn.eq(0).click()
      })
        .then(() => {
          // - check if notification appears
          cy.get('[data-cy=snackbar]').should(($snackbar) => {
            expect($snackbar).to.have.text('Saved')
          })
        })

      // - check if input value remains the same
      inputValue('input[name=address]').is('you know nothing, Jon Snow...')

      waitToSeeIfAllGood(1000)
    })

    // - Check the rest of inputs and buttons
    it('Check inputs and buttons', () => {
      inputValue('input[name=address]').edit('yahoo')
      inputValue('input[name=address').is('876 Seabring Street, Baker, Louisiana, 4442yahoo')

      // - Check Criticality status selection
      cy.get('select[name="status.criticality"]')
        .select('info').should('have.value', 'info')

      // - Check Verified status selection
      cy.get('[data-cy-radio-status="true"]').click({ force: true }).should(($radio) => {
        expect($radio.find('span.Radio-icon')).to.have.class('isChecked')
      })

      // - check if Discard works
      cy.get('[data-cy=btn-discard]').should(($btn) => {
        $btn.eq(0).click()
      })
        .then(() => {
          inputValue('input[name=address').is('876 Seabring Street, Baker, Louisiana, 4442')
          inputValue('select[name="status.criticality"]').is('warning')
          cy.get('[data-cy-radio-status="true"]').should(($radio) => {
            expect($radio.find('span.Radio-icon')).not.to.have.class('isChecked')
          })
        })

      // - Check Interval inputs
      inputValue('input[name="interval.startTime"]').change('today is today :)')
      inputValue('input[name="interval.endTime"]').change('tomorrow will be tomorrow :)')
      inputValue('input[name="interval.entryTime"]').change('yesterday was yesterday :)')

      // - check position inputs
      // - position.from.latitude
      inputValue('input[name="position.from.latitude"]').change('75.509597')
      // - position.from.longitude
      inputValue('input[name="position.from.longitude"]').change('-97.203744')
      // - position.to.latitude
      inputValue('input[name="position.to.latitude"]').change('-22.743405')
      // - position.to.longitude
      inputValue('input[name="position.to.longitude"]').change('10.017638')

      cy.get('select[name="status.criticality"]')
        .select('info').should('have.value', 'info')
      cy.get('[data-cy-radio-status="true"]').click({ force: true }).should(($radio) => {
        expect($radio.find('span.Radio-icon')).to.have.class('isChecked')
      })

      // - check if Save works
      cy.get('[data-cy=btn-save]').should(($btn) => {
        $btn.eq(1).click()
      })
        .then(() => {
          // - check all input values have the entered values
          inputValue('select[name="status.criticality"]').is('info')
          cy.get('[data-cy-radio-status="true"]').should(($radio) => {
            expect($radio.find('span.Radio-icon')).to.have.class('isChecked')
          })
          inputValue('input[name="interval.startTime"]').is('today is today :)')
          inputValue('input[name="interval.endTime"]').is('tomorrow will be tomorrow :)')
          inputValue('input[name="interval.entryTime"]').is('yesterday was yesterday :)')
          inputValue('input[name="position.from.latitude"]').is('75.509597')
          inputValue('input[name="position.from.longitude"]').is('-97.203744')
          inputValue('input[name="position.to.latitude"]').is('-22.743405')
          inputValue('input[name="position.to.longitude"]').is('10.017638')
        })
        .then(() => {
          cy.get('[data-cy=snackbar]').should(($snackbar) => {
            expect($snackbar).to.have.text('Saved')
          })
        })

      waitToSeeIfAllGood(300)
    })

    // - check if Return works
    it("should redirect to Home('/') after click on 'Return' button ", () => {
      cy.get('[data-cy=btn-return]').eq(0).find('a').click({ force: true })

      cy.location().should((loc) => {
        expect(loc.origin).to.eq('http://localhost:3000')
      })
    })
  })
})

function waitToSeeIfAllGood (value) {
  cy.wait(value)
}
