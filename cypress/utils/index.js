export function inputValue (selector) {
  return {
    clear: value => {
      cy.get(selector).focus().clear()
    },
    change: value => {
      cy.get(selector).focus().clear().type(value)
    },
    edit: value => {
      cy.get(selector).focus().type(value)
    },
    is: value => {
      cy.get(selector).should(($input) => {
        expect($input).to.have.value(value)
      })
    }
  }
}
